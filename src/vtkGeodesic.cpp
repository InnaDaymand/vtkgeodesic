#include "vtkGeodesic.h"
#include <ostream>
#include <math.h>

vtkStandardNewMacro(vtkGeodesic);

//-------------------------------------------------------------------------------

vtkGeodesic::vtkGeodesic()
{
	mMesh=vtkPolyData::New();
	mMesh->Allocate();
	mPLocator=vtkPointLocator::New();
	mVertexDistances=vtkFloatArray::New();
	mVertexDistances->Allocate(1000 );
	mKeyNeighbours=vtkPoints::New();
	mKeyNeighbours->Allocate(1000);
	mConnectedVertices=vtkIdList::New();
	mConnectedVertices->Allocate(1000);
	mCellIdList=vtkIdList::New();
	mCellIdList->Allocate(1000);
}

vtkGeodesic::~vtkGeodesic()
{
	deleteObjects();
}


void vtkGeodesic::PrintSelf(std::ostream& os, vtkIndent indent)
{
  PrintSelf(os,indent);
  os << indent << "mesh number of cells=: " << this->mMesh->GetNumberOfCells() << "\n";
}


void vtkGeodesic::deleteObjects()
{
	std::cout << "delete objects in class vtkGeodesic\n";
	mVertexDistances->Delete();
	mKeyNeighbours->Delete();
	mConnectedVertices->Delete();
	mMesh->Delete();
	mPLocator->Delete();
}

void vtkGeodesic::SetPolyData(vtkPolyData* mesh){
	mMesh=mesh;
    mPLocator->SetDataSet(mMesh);
    mPLocator->AutomaticOn();
    mPLocator->SetNumberOfPointsPerBucket(10);
    mPLocator->BuildLocator();
}

void vtkGeodesic::compute_geodesic_distances (
		float max_distance, float desired_distance, float tolerance){

	mVertexDistances->Reset();
	mVertexDistances->Initialize();
	//std::cout<< "mVertexDistances done initialization\n";
    mKeyNeighbours->Reset();
    mKeyNeighbours->Initialize();
	//std::cout<< "mKeyNeighbours done initialization\n";

    // use a 'coloring' mechanism to ensure not repeating the same points
    for (int index=0; index <  mMesh->GetNumberOfPoints(); index++)
        mVertexDistances->InsertNextValue(-1.0000);
	//std::cout<< "mVertexDistances set data to -1\n";

//    reference_point.Set(10.9,76.7, 70.7);
    int start_point_ID = mPLocator->FindClosestPoint(reference_point.GetData());
    mVertexDistances->SetValue(start_point_ID, 0.0);
	//std::cout<< "start_point_ID=" <<start_point_ID<<"\n";

    vtkSmartPointer<vtkIdList>current_iteration_IDs = vtkSmartPointer<vtkIdList>::New();
    current_iteration_IDs->Allocate(1000);
    current_iteration_IDs->Initialize();
    current_iteration_IDs->InsertNextId(start_point_ID);
 	//std::cout<< "current_iteration_IDs done make and initialize\n";

    // estimate number of iteration needed to reach the max distance needed

    bool is_first_reference_point = true;
    int index_all = 1;
    double origin[3] = {0, 0, 0};
    vtkSmartPointer<vtkIdList> next_level_list_of_points = vtkSmartPointer<vtkIdList>::New();
    next_level_list_of_points->Allocate(1000);
	//std::cout<< "next_level_list_of_points done make and initialize\n";

    while (current_iteration_IDs->GetNumberOfIds()>0){

        next_level_list_of_points->Reset();
        next_level_list_of_points->Initialize();
    	//std::cout<< "loop while, next_level_list_of_points was initialize\n";

        for( int indexes_ID=0; indexes_ID < current_iteration_IDs->GetNumberOfIds(); indexes_ID++){

        	double* point=mMesh->GetPoint(current_iteration_IDs->GetId(indexes_ID));
        	//std::cout<< "get data from mesh \n";
            reference_point.Set(point[0], point[1], point[2]);
            if (is_first_reference_point){
                origin[0] = reference_point.GetX(), origin[1]=reference_point.GetY(), origin[2]=reference_point.GetZ();
                is_first_reference_point =false;
            }

            double reference_distance = mVertexDistances->GetValue(current_iteration_IDs->GetId(indexes_ID));
        	//std::cout<< "get data from mVertexDistances \n";

           	//std::cout<< "call get_connected_points \n";
           	get_connected_points(current_iteration_IDs->GetId(indexes_ID));
           	//std::cout<< "return from  get_connected_points\n";

         	//std::cout<< "count of mConnectedVertices is "<<mConnectedVertices->GetNumberOfIds()<<"\n";

            for (int neighbour_ID_indexes=0;neighbour_ID_indexes< mConnectedVertices->GetNumberOfIds();
            		neighbour_ID_indexes++ ){
               	//std::cout<< "loop for, use mConnectedVertices\n";

				int current_point_ID = mConnectedVertices->GetId(neighbour_ID_indexes);

				double* neighbor_point = mMesh->GetPoint(current_point_ID);

				//estimate geodesic distance of neighbour point
				double geodesic_distance_from_original_point = reference_distance +
						sqrt(vtkMath::Distance2BetweenPoints(neighbor_point, reference_point.GetData()));

//                    if smaller distance exists, update to find shortest path
				if (mVertexDistances->GetValue(current_point_ID) > geodesic_distance_from_original_point){
					// update neighbour's geodesic distance value
					mVertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);
				}

//                     if this point was not visited before (we pre-set all to -1)
				if (mVertexDistances->GetValue(current_point_ID) < 0){
					// update neighbour's geodesic distance value
					mVertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);

					if (geodesic_distance_from_original_point < max_distance){
						// mark as a neighbour to compute its geodesic distance in next iteration
						next_level_list_of_points->InsertNextId(current_point_ID);
					}
				}

//                     if the point is at a distance that is similar to a key point
				if (abs(desired_distance - geodesic_distance_from_original_point) < tolerance){

//                             if the point is on the axial plane
						double distance = get_distance_plane(neighbor_point, origin, axes_x.GetData(), axes_y.GetData());
						if (distance < tolerance){
							mKeyNeighbours->InsertNextPoint(neighbor_point);
						}
				}
            }
        }

        current_iteration_IDs->Reset();
        current_iteration_IDs->DeepCopy(next_level_list_of_points);

        index_all = index_all + 1;
    }
}

void vtkGeodesic::get_connected_points(int id){

   mConnectedVertices->Reset();
   mConnectedVertices->Initialize();
   mCellIdList->Reset();
   mCellIdList->Initialize();
   //std::cout<< "get_connected_points: init\n";

   //std::cout<< "build links done\n";
   mMesh->GetPointCells(id, mCellIdList);
   //std::cout<< "get_connected_points: got data cells from Mesh count="<<mCellIdList->GetNumberOfIds() <<"\n";

   vtkIdList* pointIdList = vtkIdList :: New();
   pointIdList->Allocate(1000);
   //std::cout<< "get_connected_points: pointIdList was made and allocate\n";
   //std::cout<< "get_connected_points: begin loop for\n";
   for (int index=0; index < mCellIdList->GetNumberOfIds(); index++){
	   pointIdList->Reset();
       mMesh->GetCellPoints(mCellIdList->GetId(index), pointIdList);
       if (pointIdList->GetId(0) != id)
           mConnectedVertices->InsertNextId(pointIdList->GetId(0));
       else
           mConnectedVertices->InsertNextId(pointIdList->GetId(1));
   }
   //std::cout<< "get_connected_points: end loop for\n";
   pointIdList->Delete();
   //std::cout<< "get_connected_points: delete pointIdList, return from function\n";
}

// compute distance between point p and plane [q, qx, qy]
double vtkGeodesic::get_distance_plane(double* p, double* q, double* q_x, double* q_y){

	double n[3]={0,0,0};
    vtkMath::Cross(q_x, q_y, n);
    double normn=vtkMath::Norm(n);
    n[0] = n[0] / normn;
    n[1] = n[1] / normn;
    n[2] = n[2] / normn;
    double sub[3]={0,0,0};

    vtkMath::Subtract(p, q, sub);
    double distance = abs(vtkMath::Dot(sub, n));

    return distance;
}

