#ifndef vtkGeodesic_h
#define vtkGeodesic_h

#include "vtkFloatArray.h"
#include <vector>
#include "vtkPolyData.h"
#include "vtkPointLocator.h"
#include "vtkPoints.h"
#include "vtkVector.h"
#include <iostream>
#include <ostream>


class vtkVector3d;
class vtkPolyData;
class vtkPoints;
class vtkPointLocator;
class vtkFloatArray;
class vtkIdList;

class VTK_EXPORT vtkGeodesic: public vtkObject
{
public:
	vtkTypeMacro(vtkGeodesic,vtkObject);

	static vtkGeodesic *New();

	void PrintSelf(std::ostream& os, vtkIndent indent) override;

	void compute_geodesic_distances (
			float max_distance, float desired_distance, float tolerance);

	void SetPolyData(vtkPolyData* mesh);
	void SetReferencePoint(double x, double y, double z){reference_point.Set(x,y,z);};
	void SetAxesX(double x, double y, double z){axes_x.Set(x, y, z);};
	void SetAxesY(double x, double y, double z){axes_y.Set(x, y, z);};
	vtkFloatArray * GetVertexDistances() { return mVertexDistances; }
	vtkPoints * GetKeyNeighbours() { return mKeyNeighbours; }

protected:
	vtkGeodesic();
	~vtkGeodesic();
	void deleteObjects();
	vtkFloatArray * mVertexDistances;
	vtkPoints* mKeyNeighbours;
	vtkIdList* mConnectedVertices;
	vtkIdList* mCellIdList;
	vtkVector3d reference_point;
	vtkVector3d axes_x;
	vtkVector3d axes_y;
	vtkPolyData* mMesh;
	vtkPointLocator* mPLocator;

	void get_connected_points(int id);
	double get_distance_plane(double*, double*, double*, double*);
	void setDataPoints(double x, double y, double z);
	void setIdCells(int id0, int id1, int id2);
	void setCells();
	void makePolyData();

private:
  vtkGeodesic(const vtkGeodesic&);  // Not implemented.
  void operator=(const vtkGeodesic&);  // Not implemented.


};

#endif
